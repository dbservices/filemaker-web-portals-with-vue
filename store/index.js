export const state = () => ({
    user: {}
})

export const actions = {
    //On page first load, get the auth info from the session variable
    nuxtServerInit({ commit }, { req }) {
        if (req.session && req.session.authUser) {
            commit('SET_USER', req.session.authUser)
        }
    }
}

export const mutations = {
    SET_USER(state, payload) {
        state.user = payload
    }
}