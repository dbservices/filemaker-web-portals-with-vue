const express = require('express')
const consola = require('consola')
const axios = require('axios')
const bodyParser = require('body-parser')
const session = require('express-session')

const { Nuxt, Builder } = require('nuxt')
const app = express()


// Body parser, to access `req.body`
app.use(bodyParser.json())


// Sessions to create `req.session`
app.use(session({
    secret: 'super-secret-key',
    resave: false,
    saveUninitialized: false,
    cookie: {}
}))


// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'


//Get Auth Token
async function getAuthToken(req, res, forceRefresh = false) {
    //See if token is still good
    var timestamp = new Date().getTime()
    if (!forceRefresh && timestamp < req.session.tokenExpire && req.session.token) {
        var token = req.session.token
        axios.defaults.headers.common = { 'Authorization': `Bearer ${token}` }
        return token
    }

    //If not, get a new token
    try {
        var response = await axios({
            method: 'post',
            url: config.fmHost + "/fmi/data/vLatest/databases/" + config.fmDatabase + "/sessions",
            auth: {
                username: config.fmUsername,
                password: config.fmPassword
            },
            data: {},
            headers: {
                'Content-Type': 'application/json'
            }
        })
        var token = response.data.response.token
        req.session.token = token
        req.session.tokenExpire = timestamp + 14 * 60000;
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
        return token
    } catch (e) {
        if (e.response) {
            return res.send({ error: e.response.data.messages[0].message })
        } else {
            return res.send({ error: "Error getting token" })
        }
    }
}


async function start() {
    // Init Nuxt.js
    const nuxt = new Nuxt(config)

    const { host, port } = nuxt.options.server

    // Build only in dev mode
    if (config.dev) {
        const builder = new Builder(nuxt)
        await builder.build()
    } else {
        await nuxt.ready()
    }

    app.post('/api/login', async function(req, res) {
        try {
            var token = await getAuthToken(req, res, false)
            var scriptParam = {
                username: req.body.username,
                password: req.body.password
            }
            var payload = {
                "fieldData": {},
                "script": "login",
                "script.param": JSON.stringify(scriptParam)
            }

            //var response = await axios.post(config.fmHost + "/fmi/data/vLatest/databases/" + config.fmDatabase + "/layouts/POST/records", payload)
            var response = await axios({
                method: 'post',
                url: config.fmHost + "/fmi/data/vLatest/databases/" + config.fmDatabase + "/layouts/POST/records",
                data: payload,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            var scriptResult = JSON.parse(response.data.response.scriptResult)
            req.session.loggedIn = scriptResult.loggedIn
            req.session.authUser = scriptResult
            res.send(scriptResult)
        } catch (e) {
            if (e.response) {
                res.send({ error: e.response.data.messages[0].message })
            } else {
                res.send({ error: e })
            }
        }
    })

    app.get('/api/logout', async function(req, res) {
        try {
            req.session.loggedIn = false
            req.session.authUser = {}
            var token = await getAuthToken(req, res, false)
            var response = await axios({
                method: 'delete',
                url: config.fmHost + "/fmi/data/vLatest/databases/" + config.fmDatabase + "/sessions/" + token,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            req.session.token = null
            req.session.tokenExpire = null
            res.json({ ok: true })
        } catch (e) {
            if (e.response) {
                res.send({ error: e.response.data.messages[0].message })
            } else {
                res.send({ error: "error logging out" })
            }
        }
    })

    app.get('/api/invoices', async function(req, res) {
        try {
            if (!req.session.loggedIn) {
                throw { response: "Not logged in" }
            }
            var userId = req.session.authUser.id
            var token = await getAuthToken(req, res, false)
            var response = await axios({
                method: 'post',
                url: config.fmHost + "/fmi/data/vLatest/databases/" + config.fmDatabase + "/layouts/INVOICE/_find",
                data: {
                    query: [{ _kftUserID: userId }]
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            res.send(response.data)
        } catch (e) {
            res.send({ error: "error getting invoices" })
        }
    })

    app.get('/api/invoices/:id', async function(req, res) {
        try {
            if (!req.session.loggedIn) {
                throw { response: "Not logged in" }
            }

            var userId = req.session.authUser.id
            var token = await getAuthToken(req, res, false)
            var response = await axios({
                method: 'get',
                url: config.fmHost + "/fmi/data/vLatest/databases/" + config.fmDatabase + "/layouts/INVOICE/records/" + req.params.id,
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            //if invoice user id doesn't match user id, return error
            var invoiceUserID = response.data.response.data[0].fieldData._kftUserID
            if (invoiceUserID != userId) {
                throw { response: "You are not allowed to view this invoice" }
            }
            return res.send(response.data)
        } catch (e) {
            res.status(403)
            return res.send(e)
        }
    })




    // Give nuxt middleware to express
    app.use(nuxt.render)

    // Listen the server
    app.listen(port, host)
    consola.ready({
        message: `Server listening on http://${host}:${port}`,
        badge: true
    })
}
start()