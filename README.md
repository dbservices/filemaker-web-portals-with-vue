# DB Services - FileMaker Web Portals with Vue.js

> My stellar Nuxt.js project

[https://dbservices.com/articles/filemaker-web-portals-with-vue-js/](https://dbservices.com/articles/filemaker-web-portals-with-vue-js/)

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
