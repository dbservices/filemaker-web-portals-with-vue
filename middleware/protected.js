export default function(ctx) {
    if (!ctx.store.state.user.loggedIn) {
        return ctx.redirect({
            name: 'login',
            query: {
                redirect: ctx.route.name,
                message: "You must be logged in to view this page."
            }
        })
    }
}